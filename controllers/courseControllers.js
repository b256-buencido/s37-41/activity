const Course = require("../models/Course.js");
const auth = require("../auth.js");

// Create a new course
/*
	Business Logic:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/


module.exports.addCourse = (course) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide
	let newCourse = new Course({
		name: course.name,
		description: course.description,
		price: course.price
	});

	return newCourse.save().then((result, err) => {

		if(err) {

			return false;
		}
		else {

			return true;
		}
	})
}

// Retrieving all courses
/*
	Business Logic:
	1. Retrieve all the courses from the database
*/

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {

		return result;
	});
};

// Retrieving all active courses
/*
	Business Logic:
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/
module.exports.getActiveCourses = () => {

	return Course.find({isActive: true}).then(result => {

		return result
	})
}

// Retrieving a specific course
/*
	Business Logic:
	1. Retrieve the course that matches the course ID provided from the URL
*/

// reqParams = req.params(url) = "/:courseId"
module.exports.getCourse = (requestParams) => {

	return Course.findById(requestParams.courseId).then(result => {

		return result;
	})
}

// Updating a course
/*
	Business Logic:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/

// course = data.course = request.body
// paramsId = data.params = request.params = /:courseId
module.exports.updateCourse = (course, paramsId) => {

	// specify the field/properties that needs to be updated
	let updatedCourse = {
		name: course.name,
		description: course.description,
		price: course.price
	}

	// findByIdAndUpdate(id, updatedContent)
	return Course.findByIdAndUpdate(paramsId.courseId, updatedCourse).then((result, err) => {

		if(err) {

			return false;
		}
		else {
			return true;
		}
	})
}

// Archiving a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently

module.exports.archiveCourse = (requestParams, requestBody) => {

	let updatedActiveField = {
		isActive: requestBody.isActive
	}

	return Course.findByIdAndUpdate(requestParams.courseId, updatedActiveField).then((course, error) => {

		if (error) {

			return false;
		}
		else {

			return true
		}
	})
}