// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to connect to our frontend application
// Allows us to control the application's Cross-Origin Resource Sharing Settings
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js")


// Setup server
// Create an "app" variable that stores the result of the "express" function that allows us to access to different methods that will make the backend creation easy
const app = express();

// Middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}));

// Routes
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Database connection
mongoose.connect("mongodb+srv://admin:admin1234@b256buencido.lmfwv9e.mongodb.net/B256_CourseAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));

// Server listening
app.listen(4000, () => console.log(`API is now online on port 4000`));