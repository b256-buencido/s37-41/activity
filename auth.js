// auth.js is to Create authentication of our website
const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// JSON Web Token
/*
	- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
	- Information is kept secure through the use of the secret code
	- Only the system that knows the secret code that can decode the encrypted information

	- Imagine JWT as a gift wrapping service that secures the gift with a lock
	- Only the person who knows the secret code can open the lock
	- And if the wrapper has been tampered with, JWT also recognizes this a
	- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
	- This ensures that the data is secure from the sender to the receiver
*/

// Token creation
/*
	Analogy
		Pack the gift and provide a lock with the secret code as the key
*/

module.exports.createAccessToken = (user) => {

	// Payload
	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
}

// Token verification
/*
	Analogy
		Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/

module.exports.verify = (request, response, next) => {

	// The token is retrieved from the request header
	// This can be provided in postman under
		// Authorization > Bearer Token
	let token = request.headers.authorization

	// Token received is not undefined
	if (typeof token !== "undefined") {

		console.log(token);

		// "slice" method -> extract a particular section of a string without modifying the original string.
		// Bearer nv457nhgvh43857tf347r5783784
		// this removes the word "Bearer" including the space after to get the token value ONLY
		// token = nv457nhgvh43857tf347r5783784
		token = token.slice(7, token.length);

		// verify the token using the verify() method
		return jwt.verify(token, secret, (err, data) => {

			// if JWT is not valid
			if(err) {

				return response.send({auth: "failed"})
			}
			else {

				next();
			}
		})
	}
	else{

		return response.send({auth: "failed"})
	}
}

// Token decryption
/*
	
*/

module.exports.decode = (token) => {

	// if token is not undefined
	if(typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) {

				return null;
			}
			else {

				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else {

		return null
	}
};