const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth.js");


// Route for creating a course
router.post("/create", auth.verify, (request, response) => {
	// Method number 1
	// contains the data needed for creating a course
	const data = {

		// contains all the information needed in the function
		// data.course
		course: request.body, //name,description,price
		isAdmin: auth.decode(request.headers.authorization).isAdmin
		// to access the headers in postman, we need the request.headers.authorization
		// 
	}

	if(data.isAdmin) {

		courseController.addCourse(data.course).then(resultFromController => response.send(resultFromController));
	}
	else {

		response.send(false)
	}
	

	/*
	Method number 2
	const userData = auth.decode(request.headers.authorization);

	courseController.addCourse(request.body, userData.isAdmin).then(resultFromController => response.send(resultFromController))
	*/

	/*
	Method number 3

	const userData = auth.decode(request.headers.authorization);

	courseController.addCourse(request.body, userData.isAdmin).then(resultFromController => response.send(resultFromController))
	*/
})

// Route for retrieving all courses
router.get("/all", (request, response) => {

	courseController.getAllCourses().then(resultFromController => response.send(resultFromController));
})

// Route for retrieving all active courses
router.get("/active", (request, response) => {

	courseController.getActiveCourses().then(resultFromController => response.send(resultFromController));
})

// Route for retrieving a specific course
router.get("/:courseId", (request, response) => {

	courseController.getCourse(request.params).then(resultFromController => response.send(resultFromController));
})

// Route for updating a course
router.put("/update/:courseId", auth.verify, (request, response) => {

	const data = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		params: request.params
	}

	if(data.isAdmin) {

		courseController.updateCourse(data.course, data.params).then(resultFromController => response.send(resultFromController))
	}
	else {

		response.send(false)
	}
})

// Route for archiving a course
// A "PUT" method is used instead of "DELETE"  request because of our approach in archiving and hiding the courses from our users by "soft deleting" record instead of "hard deleting" records which removes them permanently from our databases
router.patch("/archive/:courseId", auth.verify, (request, response) => {

	const data = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		params: request.params
	}

	if(data.isAdmin) {

		courseController.archiveCourse(data.params, data.course).then(resultFromController => response.send(resultFromController))
	}
	else {

		response.send(false)
	}
})

module.exports = router;