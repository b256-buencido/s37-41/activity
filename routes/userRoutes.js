const express = require("express");
const router = express.Router(); //to access and fix the routes
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// Route for checking if the user's email already exist in the database
router.post("/checkEmail", (request, response) => {

	userController.checkIfEmailExists(request.body).then(resultFromController => response.send(resultFromController));
});

// Route for user registration
router.post("/register", (request, response) => {

	userController.registerUser(request.body).then(resultFromController => response.send(resultFromController));
});

// Route for user authentication
router.post("/login", (request, response) => {

	userController.authenticateUser(request.body).then(resultFromController => response.send(resultFromController));
});

// S38 activity
// Create a /details route that will accept the user’s Id to retrieve the details of a user.
// auth.verify acts as middleware
router.get("/details", auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	userController.getProfile({userId: userData.id}).then(resultFromController => response.send(resultFromController));
});

// Route for enrolling a user
router.post("/enroll", auth.verify, (request, response) => {

	const data = {
		// userId will be received from the request header
		userId: auth.decode(request.headers.authorization).id,
		courseId: request.body.courseId
	}

	userController.enrollUser(data).then(resultFromController => response.send(resultFromController));
})

module.exports = router;