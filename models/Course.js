// This module is for schema and models
// Setup dependencies
const mongoose = require("mongoose");

// Create the schema for our courses
const courseSchema = new mongoose.Schema({

	name: {
		type: String,
		// Requires the data for this field/property to be included when creating a record
		// the "true" value defines if the field is required or not, and the second element in the array is the message we will display in the console when the data is not present
		required: [true, "Course Name is required"]
	},
	description: {
		type: String,
		required: [true, "Course Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: String,
		// the "new Date()" is an expression that instantiates a new "date" that stores the current date and time whenever a course is created in our database
		default: new Date()
	},
	// The "enrollees" property/field will be an array of objects containing the userId and the date and time thatthe user enrolled
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

// model -> Course
// collection -> courses
// "module.exports" is a way for NodeJS to treat this value as a "package" that  can be used be other files
module.exports = mongoose.model("Course", courseSchema);